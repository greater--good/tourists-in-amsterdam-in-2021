# Tourists in Amsterdam in 2021

This repository contains documentation, data, and analysis scripts for a project aiming to minimize negative health effects related to the use of alcohol and other drugs among tourists in Amsterdam that was started in 2021 by Jellinek Amsterdam and funded by the City of Amsterdam.

The rendered R Markdown file for this project is available at https://greater--good.gitlab.io/tourists-in-amsterdam-in-2021.